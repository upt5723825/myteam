const request = require('supertest');
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('../routes/userRoutes');
const User = require('../models/User');

const app = express();
app.use(express.json());
app.use('/api/users', userRoutes);

beforeAll(async () => {
  const url = `mongodb://127.0.0.1/test_db_simple`;
  await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
});

afterAll(async () => {
  await mongoose.connection.db.dropDatabase();
  await mongoose.connection.close();
});

describe('User Routes', () => {
  let userId;

  it('create a new user', async () => {
    const userData = {
      name: 'Rares',
      surname: 'Tivadar',
      email: 'rares.tivadar@gmail.com',
      birthdate: '1990-01-01',
      role: 'player',
      password: 'password123'
    };

    const response = await request(app)
      .post('/api/users/signup')
      .send(userData);

    expect(response.status).toBe(201);
    expect(response.body.user).toMatchObject({
      name: 'Rares',
      surname: 'Tivadar',
      email: 'rares.tivadar@gmail.com',
      role: 'player'
    });

    userId = response.body.user._id;
  });

  it('get user details', async () => {
    const response = await request(app)
      .get(`/api/users/${userId}`);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({
      _id: userId,
      name: 'Rares',
      surname: 'Tivadar',
      email: 'rares.tivadar@gmail.com',
      role: 'player'
    });
  });

  it('update a user', async () => {
    const updateData = {
      name: 'Rares Updated'
    };

    const response = await request(app)
      .put(`/api/users/${userId}`)
      .send(updateData);

    expect(response.status).toBe(200);
    expect(response.body.name).toBe('Rares Updated');
  });

  it('delete a user', async () => {
    const response = await request(app)
      .delete(`/api/users/${userId}`);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('User successfully deleted.');
  });
});
